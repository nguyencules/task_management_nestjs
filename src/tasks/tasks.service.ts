import { Injectable, NotFoundException } from '@nestjs/common';

import {  TaskStatus } from './task-status.enum';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTaskFilterDto } from './dto/get-task-filter.dto';
import { TaskRepository } from './task.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './task.entity';
import { User } from 'src/auth/user.entity';
import { userInfo } from 'os';

@Injectable()
export class TasksService {
    constructor(

        @InjectRepository(TaskRepository)
        private taskRepository: TaskRepository,
    ) {}

    getTasks(
        filterDto: GetTaskFilterDto,
        user: User,
    ): Promise<Task[]>{
        return this.taskRepository.getTasks(filterDto, user);
    }
    // getAllTask(): Task[] {
    //     return this.tasks;
    // }

    // getTasksWithFilter(filterDto: GetTaskFilterDto): Task[] {
    //     const {status, search} = filterDto;
    //     let tasks = this.getAllTask();

    //     if(status) {
    //         tasks = tasks.filter((task: Task) => task.status === status);
    //     }

    //     if(search) {
    //         tasks = tasks.filter((task: Task) =>
    //             task.title.includes(search) ||
    //             task.description.includes(search),
    //         );
    //     }
    //     return tasks;
    // }

    async getTaskById( 
        id: number,
        user: User
    ): Promise<Task>{
        const found =  await this.taskRepository.findOne({ where: { id, userId: user.id}});
        
        if(!found) {
            throw new NotFoundException(`Task with ID: "${id}" notfound!`);
        }

        return found;
    }

    async createTask( 
        createTaskDto: CreateTaskDto,
        user: User
    ) {
        return this.taskRepository.createTask(createTaskDto, user);  
    }

    async deleteTask(
        id: number,
        user: User
    ) {
        const rs = await this.taskRepository.delete({ id, userId: user.id });

        if (rs.affected === 0 ) {
            throw new NotFoundException(`Task with ID: "${id}" notfound!`);
        }
    }

    async updateTaskStatus(
        id: number, 
        status: TaskStatus,
        user: User
    ): Promise<Task>{
        const task = await this.getTaskById(id, user);
        task.status = status;
        await task.save();
        return task;
    }
    // updateTaskStatus( id: string, status: TaskStatus): Task {
    //     const task = this.getTaskById(id);
    //     task.status = status;
    //     return task;
    // }
    
}
