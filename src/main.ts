import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';
import * as config from 'config';

async function bootstrap() {
  const severConfig = config.get('sever');
  const logger = new Logger();
  const app = await NestFactory.create(AppModule);

  const port = process.env.PORT || severConfig.port;
  await app.listen(port);
  logger.log(`Application listening on Port ${port}`);
}
bootstrap();
